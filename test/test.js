const { assert } = require('chai');
const { newUser } = require('../index.js');

//describe gives structure to your test suite
describe('Test newUser Object', () => {

    //unit test
    it('Asset newUser type is object', () => {

        assert.equal(typeof(newUser),'object')

    })

    it('Assert newUser.email is type string',() => {

        assert.equal(typeof(newUser.email),'string')

    })

    it('Assert newUser type is not equal to undefined',() => {

        assert.notEqual(typeof(newUser.email),undefined);

    })
    //.notEqual = testing for inequality !=

    it('Assert newUser.password is a string',() => {
        
        assert.equal(typeof(newUser.password),'string')

    })

    it('Assert that the number of characters in newUser.password is at least 16',() => {

        //.isAtLeast = testing for greater than or equal to >=
        assert.isAtLeast(newUser.password.length,16)

    })

    //ACTIVITY CODE

    it('Assert that the newUser firstName type is a string',() => {

        assert.equal(typeof(newUser.firstName),'string');

    });

    it('Assert that the newUser lastName type is a string',() =>{

        assert.equal(typeof(newUser.lastName),'string');
    });

    it('Assert that the newUser firstName is not undefined',() => {

        assert.notEqual(typeof(newUser.firstName),undefined);
    })

    it('Assert that the newUser lastName is not undefined',() => {

        assert.notEqual(typeof(newUser.lastName),undefined);
    })

    it('Assert that the newUser age is at least 18',() => {

        assert.isAtLeast(newUser.age,18);
    });

    it('Assert that the newUser age type is a number',() => {

        assert.equal(typeof(newUser.age),'number')
    })

    it('Assert that newUser contact number type is a string',() => {

        assert.equal(typeof(newUser.contactNumber),'string');
    })

    it('Assert that newUser contact number is at least 11 characters',() => {

        assert.isAtLeast(newUser.contactNumber.length,11);
    })

    it('Assert that newUser batch number type is a number',() => {

        assert.equal(typeof(newUser.batchNumber),'number');
    })

    it('Assert that newUser batch number is not undefined',() => {

        assert.notEqual(typeof(newUser.batchNumber),undefined);
    })

});